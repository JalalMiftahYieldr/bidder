package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/alexkappa/bidder/impulse"
	"bitbucket.org/alexkappa/bidder/openrtb"
)

//var creativeSizeFiltering map[string][]*impulse.Campaign

var creativeSizeFiltering = make(map[string][]*impulse.Campaign)

func main() {
	handler := &BidHandler{Impulse: impulse.New()}
	handler.Impulse.Sync()

	// At this point, we have the campaigns and the creatives
	log.Println("Number of campaigns " + strconv.Itoa(len(handler.Impulse.Campaigns)))
	log.Println("Number of creatives " + strconv.Itoa(len(handler.Impulse.Creatives)))

	// Populate new datastructure filtering map "width*height"
	//creativeSizeFiltering := make(map[string][]*impulse.Campaign)

	populateCreativeFilter(handler, creativeSizeFiltering)
	log.Print(creativeSizeFiltering)

	var host = flag.String("hostname", "localhost:8080", "hostname:port")
	flag.Parse()

	http.HandleFunc("/openrtb", handler.ServeOpenRTB)
	http.HandleFunc("/rubicon", handler.ServeRubicon)

	err := http.ListenAndServe(*host, nil)
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func populateCreativeFilter(handler *BidHandler, filter map[string][]*impulse.Campaign) {
	for _, creative := range handler.Impulse.Creatives {
		size := strconv.FormatInt(creative.Width, 10) + "x" + strconv.FormatInt(creative.Height, 10)
		//if val, ok := filter[size]; ok {
		// The key already exists in the table, we append the campaigns pointers in there
		for _, campaign := range creative.Campaigns {
			exists := false
			c := lookupCampaignById(handler, campaign.Id)
			for _, camp := range filter[size] {
				if c == camp {
					exists = true
				}
			}
			if c != nil && !exists {
				filter[size] = append(filter[size], c)
			}

		}
	}
}

func lookupCampaignById(handler *BidHandler, id int64) *impulse.Campaign {
	for _, c := range handler.Impulse.Campaigns {
		if id == c.Id {
			return c
		}
	}
	return nil
}

func lookupCreativeById(handler *BidHandler, id int64) *impulse.Creative {
	for _, c := range handler.Impulse.Creatives {
		if id == c.Id {
			return c
		}
	}
	return nil
}

type BidHandler struct {
	*impulse.Impulse
}

func (h *BidHandler) ServeRubicon(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Now serving Rubicon Project RTB Bid Requests..."))
}

func (h *BidHandler) ServeOpenRTB(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	breq := new(openrtb.BidRequest)
	json.Unmarshal(body, breq)
	log.Println("request received " + breq.Id)

	eligible := h.FilterEligible(breq, creativeSizeFiltering)

	res, err := json.Marshal(eligible)
	if err != nil {
		panic(err)
	}

	w.Header().Add("Blip", "Shabloimps!")
	w.Header().Add("Server", "Impulse")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func (h *BidHandler) FilterEligible(breq *openrtb.BidRequest, filter map[string][]*impulse.Campaign) *openrtb.BidResponse {

	//eligible := make([]map[string]interface{}, 0, 100)

	// filter size

	log.Print(len(filter))

	for _, impression := range breq.Impressions {
		w := impression.Banner.Width
		he := impression.Banner.Height
		size := strconv.FormatInt(w, 10) + "x" + strconv.FormatInt(he, 10)
		if campaigns, ok := filter[size]; ok {

			log.Println("filter succeed.")
			// just select the first one campaign & creative
			c := campaigns[0]
			creativeid := c.Creatives[0].Id
			creative := lookupCreativeById(h, creativeid)

			res := new(openrtb.BidResponse)
			res.SeatBids = make([]openrtb.SeatBid, 1)
			res.SeatBids[0].Bids = make([]openrtb.Bid, 1)
			res.SeatBids[0].Bids[0].Price = 1.0
			res.SeatBids[0].Bids[0].AdMarkup = creative.AdMarkup()
			// e := map[string]interface{}{
			// 	"id":   c.Id,
			// 	"name": c.Name,
			// 	"geo":  c.Targeting.Geo,
			// }

			// eligible = append(eligible, e)
			log.Println(creative)
			return res
		}
	}

	// for _, c := range h.Campaigns {
	// 	country := breq.User.Geo.Country
	// 	region := breq.User.Geo.Region
	// 	if c.Targeting != nil {
	// 		if targets, _ := c.Targeting.Geo.Targets(country, region); targets {
	// 			e := map[string]interface{}{
	// 				"id":   c.Id,
	// 				"name": c.Name,
	// 				"geo":  c.Targeting.Geo,
	// 			}
	// 			eligible = append(eligible, e)
	// 		}
	// 	}
	// }

	return nil
}
