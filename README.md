# Impulse RTB Bidder

This project is a stepping stone into building our very own [Real Time Bidder][rtb].

# Usage

```bash
$ git clone https://bitbucket.org/alexkappa/bidder
$ make
$ ./bidder
```

Wait for it to sync some campaigns, and then use `curl` to POST a bid request.

```bash
curl -d @openrtb/examples/request.json bidder.localhost.com:8080/openrtb
```

[rtb]: http://en.wikipedia.org/wiki/Real-time_bidding