SHELL = /bin/sh

GOBLD=go build
GOTEST=go test
GOINST=go install

build:
	cd openrtb; ${GOBLD}
	cd impulse; ${GOBLD}
	${GOBLD}

test:
	cd openrtb; ${GOTEST}
	cd impulse; ${GOTEST}
	${GOTEST}

install:
	${GOINST}