// Object Definitions as described by the OpenRTB API Specification Version 2.1
package openrtb

type BidRequest struct {
	// Unique ID of the bid request, provided by the exchange.
	Id string `json:"id"`

	// Array of impression objects. Multiple impression auctions may be
	// specified in a single bid request. At least one impression is required
	// for a valid bid request
	Impressions []Impression `json:"imp"`

	// See Site Object
	Site Site `json:"site"`

	// See App Object
	App App `json:"app"`

	// See Device Object
	Device Device `json:"device"`

	// See User Object
	User User `json:"user"`

	// Auction Type. If "1", then first￼price auction. If "2", then second price
	// auction. Additional auction types can be defined as per the exchange’s
	// business rules. Exchange specific
	AuctionType uint8 `json:"at"`

	// Maximum amount of time in milliseconds to submit a bid (e.g., 120 means
	// the bidder has 120ms to submit a bid before the auction is complete). If
	// this value never changes across an exchange, then the exchange can supply
	// this information offline
	TMax int64 `json:"tmax"`

	// Array of buyer seats allowed to bid on this auction. Seats are an
	// optional feature of exchange. For example, ["4","34","82","A45"]
	// indicates that only advertisers using these exchange seats are allowed to
	// bid on the impressions in this auction.
	Seats []string `json:"wseat"`

	// Flag to indicate whether Exchange can verify that all impressions offered
	// represent all of the impressions available in context (e.g., all
	// impressions available on the web page; all impressions available for a
	// video [pre, mid and postroll spots], etc.) to support road-blocking. A
	// true value should only be passed if the exchange is aware of all
	// impressions in context for the publisher. "0" means the exchange cannot
	// verify, and "1" means that all impressions represent all impressions
	// available
	AllImpressions uint8 `json:"allimps"`

	// Array of allowed currencies for bids on this bid request using ISO-4217
	// alphabetic codes. If only one currency is used by the exchange, this
	// parameter is not required
	Currencies []string `json:"cur"`

	// Blocked Advertiser Categories. Note that there is no existing
	// categorization / taxonomy of advertiser industries. However, as a
	// substitute exchanges may decide to use IAB categories as an approximation
	BlockedAdvertiserCategories []string `json:"bcat"`

	// Array of strings of blocked top- level domains of advertisers. For
	// example, {"company1.com", "company2.com"}
	BlockedAdvertisers []string `json:"badv"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}

type Impression struct {
	// A unique identifier for this impression within the context of the bid
	// request (typically, value starts with 1, and increments up to n for n
	// impressions)
	Id string `json"id"`

	// A reference to a banner object. Either a banner or video object (or both
	// if the impression could be either) must be included in an impression
	// object. See Banner Object
	Banner Banner `json:"banner"`

	// A reference to a video object. Either a banner or video object (or both
	// if the impression could be either) must be included in an impression
	// object. See Video Object
	Video Video `json:"video"`

	// Name of ad mediation partner, SDK technology, or native player
	// responsible for rendering ad (typically video or mobile). Used by some ad
	// servers to customize ad code by partner
	DisplayManager string `json:"displaymanager"`

	// Version of ad mediation partner, SDK technology, or native player
	// responsible for rendering ad (typically video or mobile). Used by some ad
	// servers to customize ad code by partner
	DisplayManagerVersion string `json:"displaymanagerver"`

	// 1 if the ad is interstitial or full screen; else 0 (i.e., no)
	Interstitial int64 `json:"instl"`

	// Identifier for specific ad placement or ad tag that was used to initiate
	// the auction. This can be useful for debugging of any issues, or for
	// optimization by the buyer
	TagId string `json:"tagid"`

	// Bid floor for this impression (in CPM of bidfloorcur)
	BidFloor float64 `json:"bidfloor"`

	// If bid floor is specified and multiple currencies supported per bid
	// request, then currency should be specified here using ISO-4217 alphabetic
	// codes. Note, this may be different from bid currency returned by bidder,
	// if this is allowed on an exchange
	BidFloorCurrency string `json:"bidfloorcur"`

	// Array of names for supported iframe busters. Exchange specific
	IFrameBuster []string `json:"iframebuster"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}

type Banner struct {
	// Unique identifier for this banner object. Useful for tracking multiple
	// banner objects (e.g., in companion banner array). Usually starts with 1,
	// increasing with each object. Combination of impression id banner object
	// should be unique.
	Id string `json:"id"`

	// Width of the impression in pixels. Since some ad types are not restricted
	// by size this field is not required, but it’s highly recommended that this
	// information be included when possible
	Width int64 `json:"w"`

	// Height of the impression in pixels. Since some ad types are not
	// restricted by size this field is not required, but it’s highly
	// recommended that this information be included when possible
	Height int64 `json:"h"`

	// Ad Position. Use Table 6.5
	Position int64 `json:"pos"`

	// Blocked creative types. See Table 6.2 Banner Ad Types. If blank, assume
	// all types are allowed
	BlockedCreativeTypes []int `json:"btype"`

	// Blocked creative attributes. See Table 6.3 Creative Attributes. If blank
	// assume all types are allowed
	BlockedCreativeAttributes []int `json:"battr"`

	// Whitelist of content MIME types supported. Popular MIME types include,
	// but are not limited to "image/jpg", "image/gif" and
	// "application/x-shockwave-flash"
	Mimes []string `json:"mimes"`

	// Specify if the banner is delivered in he top frame or in an iframe. "0"
	// means it is not in the top frame, and "1" means that it is
	TopFrame uint8 `json:"topframe"`

	// Specify properties for an expandable ad. See Table 6.11 Expandable
	// Direction for possible values
	ExpandableDirection []int `json:"expdir"`

	// List of supported API frameworks for this banner. (See Table 6.4 API
	// frameworks). If an API is not explicitly listed it is assumed not to be
	// supported
	Api []int `json:"api"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}

type Video struct {
	// Content MIME types supported. Popular MIME types include, but are not
	// limited to "video/x-ms-wmv" for Windows Media, and "video/x-flv" for
	// Flash Video
	Mimes []string `json:"mimes"`

	// Indicates whether the ad impression is linear or non-linear. See Table
	// 6.6 Video Linearity for list of the possible values
	Linearity int64 `json:"linearity"`

	// Minimum video ad duration in seconds
	MinDuration int64 `json:"minduration"`

	// Maximum video ad duration in seconds
	MaxDuration int64 `json:"maxduration"`

	// Video bid response protocols. See Table 6.7 Video Bid Response Protocols
	// for a list of possible values
	Protocol int64 `json:"protocol"`

	// Width of the player in pixels. This field is not required, but it's
	// highly recommended that this information be included
	Width int64 `json:"w"`

	// Height of the player in pixels. This field is not required, but it's
	// highly recommended that this information be included
	Height int64 `json:"h"`

	// Indicates the start delay in seconds for preroll, midroll, or postroll
	// ad placement. See Table .9 Video Start Delay for generic placement values
	StartDelay int64 `json:"startdelay"`

	// If multiple ad impressions are offered in the same bid request, the
	// sequence number will allow for the coordinated delivery of multiple
	// creatives
	Sequence int64 `json:"sequence"`

	// Blocked creative attributes. See Table 6.3 Creative Attributes. If blank
	// assume all types are allowed
	BlockedCreativeAttributes []int `json:"battr"`

	// Maximum extended video ad duration, if extension is allowed. If blank or
	// 0, extension is not allowed. If -1, extension is allowed, and there is no
	// time limit imposed. If greater than 0, hen the value represents the
	// number of seconds of extended play supported beyond the maxduration value
	MaxExtended int64 `json:"maxextended"`

	// Minimum bit rate in Kbps. Exchange may set this dynamically, or
	// universally across their set of publishers
	MinBitrate int64 `json:"minbitrate"`

	// Maximum bit rate in Kbps. Exchange may set this dynamically, or
	// universally across their set of publishers
	MaxBitrate int64 `json:"maxbitrate"`

	// If exchange publisher has rules preventing letter boxing of 4x3 content
	// to play in a 16x9 window, then this should be set to false. Default
	// setting is true, which assumes that boxing of content to fit into a
	// window is allowed. "1" indicates boxing is allowed. "0" indicates it is
	// not allowed
	BoxingAllowed uint8 `json:"boxingallowed"`

	// List of allowed playback methods f blank, assume that all are allowed.
	// See Table 6.8 Video Playback Methods for a list of possible values
	PlaybackMethod []int `json:"playbackmethod"`

	// List of supported delivery methods (streaming, progressive). If blank,
	// assume all are supported. See Table 6.12 content Delivery Methods for a
	// list of possible values
	Delivery []int `json:"delivery"`

	// Ad Position (see table 6.5)
	Position int64 `json:"pos"`

	// If companion ads are available, they can be listed as an array of banner
	// objects. See Banner Object
	CompanionAd []Banner `json:"companionad"`

	// List of supported API frameworks for this impression. (See Table 6.4 API
	// Frameworks). If an API is not explicitly listed it is assumed not to be
	// supported
	Api []int `json:"api"`

	// Recommended if companion objects are included. See Table 6.17 VAST
	// Companion Types for a list of possible values
	CompanionType []int `json:"companiontype"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}

type Site struct {
	// Site ID on the exchange
	Id string `json:"id"`

	// Site name (may be masked at publisher’s request)
	Name string `json:"name"`

	// Domain of the site, used for advertiser side blocking. E.g. "foo.com"
	Domain string `json:"domain"`

	// Array of IAB content categories for the overall site. See Table 6.1
	// Content Categories
	Categories []string `json:"cat"`

	// Array of IAB content categories for the current subsection of the site.
	// See Table 6.1 Content Categories
	SectionCategory []string `json:"sectioncat"`

	// Array of IAB content categories for the current page. See Table 6.1
	// Content Categories
	PageCategory []string `json:"pagecat"`

	// URL of the page where the impression will be shown.
	Page string `json:"page"`

	// Specifies whether the site has a privacy policy. "1" means there is a
	// policy. "0" means there is not
	PrivacyPolicy uint8 `json:"privacypolicy"`

	// Referrer URL that caused navigation to the current page
	Referrer string `json:"ref"`

	// Search string that caused navigation to the current page
	Search string `json:"search"`

	// See Publisher Object
	Publisher Publisher `json:"publisher"`

	// See Content Object
	Content Content `json:"content"`

	// List of keywords describing this site in a comma separated string
	Keywords string `json:"keywords"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined
	Extra interface{} `json:"ext"`
}

type App struct {
	// Application ID on the exchange
	Id string `json:"id"`

	// Application name (may be masked at publisher’s request)
	Name string `json:"name"`

	// Domain of the application (e.g., "mygame.foo.com")
	Domain string `json:"domain"`

	// Array of IAB content categories for the overall application. See Table
	// 6.1 Content Categories
	Category []string `json:"cat"`

	// Array of IAB content categories for the current subsection of the app.
	// See Table 6.1 Content Categories
	Sectioncat []string `json:"sectioncat"`

	// Array of IAB content categories for the current page/view of the app. See
	// Table 6.1 Content Categories
	PageCategory []string `json:"pagecat"`

	// Application version
	Version string `json:"ver"`

	// Application bundle or package name (e.g., "com.foo.mygame"). This is
	// intended to be a unique ID across multiple exchanges
	Bundle string `json:"bundle"`

	// Specifies whether the app has a privacy policy. "1" means there is a
	// policy and "0" means there is not
	PrivacyPolicy uint8 `json:"privacypolicy"`

	// "1" if the application is a paid version; else "0" (i.e., free)
	Paid uint8 `json:"paid"`

	// See Publisher Object
	Publisher Publisher `json:"publisher"`

	// See Content Object
	content Content `json:"content"`

	// List of keywords describing this app in a comma separated string
	Keywords string `json:"keywords"`

	// For QAG 1.5 compliance, an app store URL for an installed app should be
	// passed in the bid request
	StoreURL string `json:"storeurl"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in the specification
	Extra interface{} `json:"ext"`
}

type Content struct {
	// ID uniquely identifying the content
	Id string `json:"id"`

	// Content episode number (typically applies to video content)
	Episode int64 `json:"episode"`

	// Content title. Video examples: "Search Committee" (television) or "A New
	// Hope" (movie) or "Endgame" (made for web) Non-video example: "Why an
	// Antarctic Glacier Is Melting So Quickly" (Time magazine article)
	Title string `json:"title"`

	// Content series. Video examples: "The Office" (television) or "Star Wars"
	// (movie) or "Arby 'N' The Chief" (made for web) Non-video example:
	// "Ecocentric" (Time magazine blog)
	Series string `json:"series"`

	// Content season. E.g., "Season 3" (typically applies to video content)
	Season string `json:"season"`

	// Original URL of the content, for buy-side contextualization or review
	URL string `json:"url"`

	// Array of IAB content categories for the content. See Table 6.1 Content
	// Categories
	Category []string `json:"cat"`

	// Video quality per the IAB’s classification. See Table 6.14 Video Quality
	VideoQuality int64 `json:"videoquality"`

	// Comma separated list of keywords describing the content
	Keywords string `json:"keywords"`

	// Content rating (e.g., MPAA)
	ContentRating string `json:"contentrating"`

	// User rating of the content (e.g., number of stars, likes, etc.).
	UserRating string `json:"userrating"`

	// Specifies the type of content (game, video, text, etc.). See Table 6.13
	// Content Context
	Context string `json:"context"`

	// Is content live? E.g., live video stream, live blog. "1" means content is
	// live. "0" means it is not live
	LiveStream uint8 `json:"livestream"`

	// 1 for "direct"; 0 for "indirect"
	SourceRelationship uint8 `json:"sourcerelationship"`

	// See Producer Object
	Producer Producer `json:"producer"`

	// Length of content (appropriate for video or audio) in seconds
	Length int64 `json:"len"`

	// Media rating of the content, per QAG guidelines. See Table 0 QAG Media
	// Ratings for list of possible values
	QAGMediaRating uint8 `json:"qagmediarating"`

	// From QAG Video Addendum. If content can be embedded (such as an
	// embeddable video player) this value should be set to "1". If content
	// cannot be embedded, then this should be set to "0"
	Embeddable uint8 `json:"embeddable"`

	// Language of the content. Use alpha-2/ISO 639-1 codes
	Language string `json:"language"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}

type Publisher struct {
	// Publisher ID on the exchange
	Id string `json:"id"`

	// Publisher name (may be masked at publisher’s request)
	Name string `json:"name"`

	// Array of IAB content categories for the publisher. See Table 6.1 Content
	// Categories
	Category []string `json:"cat"`

	// Publisher's highest level domain name, for example "foopub.com"
	Domain string `json:"domain"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}

type Producer struct {
	// Content producer or originator ID. Useful if content is syndicated, and
	// may be posted on a site using embed tags
	Id string `json:"id"`

	// Content producer or originator name (e.g., "Warner Bros")
	Name string `json:"name"`

	// Array of IAB content categories for the content producer. See Table 6.1
	// Content Categories
	Category []string `json:"cat"`

	// URL of the content producer
	Domain string `json:"domain"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}

type Device struct {
	// If "0", then do not track Is set to false, if "1", then do no track is
	// set to true in browser
	DoNotTrack uint8 `json:"dnt"`

	// Browser user agent string
	UserAgent string `json:"ua"`

	// IPv4 address closest to device
	Ip string `json:"ip"`

	// Geography as derived from the device's location services (e.g., cell
	// tower triangulation, GPS) or IP address
	Geo Geo `json:"geo"`

	// SHA1 hashed device ID; IMEI when available, else MEID or ESN. OpenRTB's
	// preferred method for device ID hashing is SHA1
	DeviceIdSha1 string `json:"didsha1"`

	// MD5 hashed device ID; IMEI when available, else MEID or ESN. Should be
	// interpreted as case insensitive
	DeviceIdMd5 string `json:"didmd5"`

	// SHA1 hashed platform-specific ID (e.g., Android ID or UDID for iOS).
	// OpenRTB's preferred method for device ID hash is SHA1
	PlatformSpecificIdSha1 string `json:"dpidsha1"`

	// MD5 hashed platform-specific ID (e.g., Android ID or UDID for iOS).
	// Should be interpreted as case insensitive
	PlatformSpecificIdMd5 string `json:"dpidmd5"`

	// IP address in IPv6
	Ipv6 string `json:"ipv6"`

	// Carrier or ISP derived from the IP address. Should be specified using
	// Mobile Network Code http://en.wikipedia.org/wiki/Mobile_Network_Code
	Carrier string `json:"carrier"`

	// Browser language; use alpha-2/ISO 639-1 codes
	Language string `json:"language"`

	// Device make (e.g., "Apple")
	Make string `json:"make"`

	// Device model (e.g., "iPhone")
	Model string `json:"model"`

	// Device operating system (e.g., "iOS")
	Os string `json:"os"`

	// Device operating system version (e.g., "3.1.2")
	OsVersion string `json:"Osv"`

	// "1" if the device supports JavaScript; else "0"
	JavaScript uint8 `json:"Js"`

	// Return the detected data connection type for the device. See Table 6.10
	// Connection Type
	ConnectionType uint8 `json:"connectiontype"`

	// Return the device type being used. See Table 6.16 Device Type
	DeviceType uint8 `json:"devicetype"`

	// Return the Flash version detected
	FlashVersion string `json:"flashver"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}

type Geo struct {
	// Latitude from -90 to 90. South is negative. This should only be passed if
	// known to be accurate (For example, not the centroid of a postal code)
	Latitude float64 `json:"lat"`

	// Longitude from -180 to 180. West is negative. This should only be passed
	// if known to be accurate
	Longitude float64 `json:"lon"`

	// Country using ISO-3166-1 Alpha-3
	Country string `json:"country"`

	// Region using ISO 3166-2
	Region string `json:"region"`

	// Region of a country using fips 10-4 notation (alternative to ISO 3166-2)
	RegionFIPS string `json:"regionfips104"`

	// Metro codes are similar to but not exactly the same as Nielsen DMAs.
	// See http://code.google.com/apis/adwords/docs/appendix/metrocodes.html)
	Metro string `json:"metro"`

	// City using United Nations Code for Trade and Transport Locations
	// (http://www.unece.org/cefact/locode/service/location.htm)
	City string `json:"city"`

	// Zip/postal code
	Zip string `json:"zip"`

	// Indicate the source of the geo data (GPS, IP address, user provided). See
	// Table 6.15 Location Type for a list of potential values. Type should be
	// provided when lat/lon is provided
	Type uint8 `json:"type"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard
	Extra interface{} `json:"ext"`
}

type User struct {
	// Unique consumer ID of this user on the exchange
	Id string `json:"id"`

	// Buyer’s user ID for this user as mapped by exchange for the buyer
	BuyersUserId string `json:"buyeruid"`

	// Year of birth as a 4-digit integer
	YearOfBirth uint8 `json:"yob"`

	// Gender as “M” male, “F” female, “O” Other. (Null indicates unknown)
	Gender rune `json:"gender"`

	// Comma separated list of keywords of consumer interests or intent
	Keywords string `json:"keywords"`

	// If supported by the exchange, this is custom data that the bidder had
	// stored in the exchanges cookie. The string may be in base85 cookie safe
	// characters, and be in any format. This may useful for storing user
	// features. Note: Proper JSON encoding must be used to include "escaped"
	// quotation marks
	CustomData string `json:"customdata"`

	// Home geo for the user (e.g., based off of registration data); this is
	// different from the current location of the access device (that is defined
	// by the geo object embedded in the Device Object); see Error! Reference
	// source not found
	Geo Geo `json:"geo"`

	// See Data Object
	Data []Data `json:"data"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}

type Data struct {
	// Exchange specific ID for the data provider
	Id string `json:"id"`

	// Data provider name
	Name string `json:"name"`

	// Array of segment objects
	Segments []Segment `json:"segment"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}

type Segment struct {
	// ID of a data provider's segment applicable to the user
	Id string `json:"id"`

	// Name of a data provider's segment applicable to the user
	Name string `json:"name"`

	// String representing the value of the segment. The method for transmitting
	// this data should be negotiated offline with the data provider. For
	// example for gender, "male", or "female", for age, "30-40")
	Value string `json:"value"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification
	Extra interface{} `json:"ext"`
}
