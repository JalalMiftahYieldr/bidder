// Object Definitions as described by the OpenRTB API Specification Version 2.1
package openrtb

type BidResponse struct {
	// ID of the bid request.
	Id string `json:"id"`

	// See SeatBid Object
	SeatBids []SeatBid `json:"seatbid"`

	// Bid response ID to assist tracking for bidders. This value is chosen by
	// the bidder for cross-reference.
	BidId string `json:"bidid"`

	// Bid currency using ISO-4217 alphabetic codes; default is "USD".
	Currency string `json:"cur"`

	// This is an optional feature, which allows a bidder to set data in the
	// exchanges cookie. The string may be in base85 cookie safe characters,
	// and be in any format. This may be useful for storing user features. Note:
	// Proper JSON encoding must be used to include "escaped" quotation marks.
	CustomData string `json:"customdata"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification.
	Extra interface{} `json:"ext"`
}

type SeatBid struct {
	// Array of bid objects; each bid object relates to an imp object in the bid
	// request. Note that, if supported by an exchange, one imp object can have
	// many bid objects.
	Bids []Bid `json:"bid"`

	// ID of the bidder seat on whose behalf this bid is made.
	Seat string `json:"seat"`

	// "1" means impressions must be won- lost as a group; default is "0".
	Group uint8 `json:"group"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification.
	Extra interface{} `json:"ext"`
}

type Bid struct {
	// ID for the bid object chosen by the bidder for tracking and debugging
	// purposes. Useful when multiple bids are submitted for a single impression
	// for a given seat.
	Id string `json:"id"`

	// ID of the impression object to which this bid applies.
	ImpressionId string `json:"impid"`

	// Bid price in CPM. WARNING/Best Practice Note: Although this value is a
	// float, OpenRTB strongly suggests using integer math for accounting to
	// avoid rounding errors.
	Price float64 `json:"price"`

	// ID that references the ad to be served if the bid wins.
	AdId string `json:"adid"`

	// Win notice URL. Note that ad markup is also typically, but not
	// necessarily, returned via this URL.
	NoticeURL string `json:"nurl"`

	// Actual ad markup. XHTML if a response to a banner object, or VAST XML if
	// a response to a video object.
	AdMarkup string `json:"adm"`

	// Advertiser’s primary or top-level domain for advertiser checking. This
	// can be a list of domains if there is a rotating creative. However,
	// exchanges may mandate that only one landing domain is allowed.
	AdDomains []string `json:"adomain"`

	// Sample image URL (without cache busting) for content checking.
	ImageURL string `json:"iurl"`

	// Campaign ID or similar that appears within the ad markup
	CampaignId string `json:"cid"`

	// Creative ID for reporting content issues or defects. This could also be
	// used as a reference to a creative ID that is posted with an exchange.
	CreativeId string `json:"crid"`

	// Array of creative attributes.
	Attributes []int64 `json:"attr"`

	// This object is a placeholder that may contain custom JSON agreed to by
	// the parties in an OpenRTB transaction to support flexibility beyond the
	// standard defined in this specification.
	Extra interface{} `json:"ext"`
}
