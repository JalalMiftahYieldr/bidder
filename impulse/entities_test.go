package impulse

import (
	"testing"
)

var (
	includes, excludes map[string]Filter
)

func init() {
	includes = map[string]Filter{
		"a": Filter{Include: []string{"a", "b", "c"}}, // Include a, b, c
		"x": Filter{Exclude: []string{"d", "e"}},      // Exclude d, e
		"y": Filter{Exclude: []string{"f", "g", "h"}}, // Exclude f, g, h
	}
	excludes = map[string]Filter{
		"d": Filter{Include: []string{"a", "b", "c"}}, // Include a, b, c
		"f": Filter{Include: []string{"d", "e"}},      // Exclude d, e
		"g": Filter{Exclude: []string{"f", "g", "h"}}, // Exclude f, g, h
	}
}

func TestIncluded(t *testing.T) {
	for value, filter := range includes {
		targets, _ := filter.Targets(value)
		t.Logf("{%s}: {%v} {%b}\n", value, filter, targets)
		if !targets {
			t.Errorf("Value {%s} should target filter {%v}\n", value, filter)
		}
	}
}

func TestExcluded(t *testing.T) {
	for value, filter := range excludes {
		targets, _ := filter.Targets(value)
		t.Logf("{%s}: {%v} {%b}\n", value, filter, targets)
		if targets {
			t.Errorf("Value {%s} should not target filter {%v}\n", value, filter)
		}
	}
}
