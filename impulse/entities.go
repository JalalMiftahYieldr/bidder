package impulse

import (
	"errors"
	"time"
)

// All response structs should contain a `meta` field containing query, order
// and paging options, and a `data` field containing an array of structs.
type Envelope interface {
	Data() interface{}
}

// Envelope implementation for the campaigns resource.
type CampaignEnvelope struct {
	Meta struct {
		Query struct {
			Where   interface{} `json:"where"`
			Limit   int         `json:"limit"`
			Offset  int         `json:"offset"`
			OrderBy string      `json:"orderby"`
			Order   string      `json:"order"`
		} `json:"query"`
		Total    int `json:"total"`
		Received struct {
			From int `json:"from"`
			To   int `json:"to"`
		} `json:"received"`
	} `json:"meta"`

	Data []*Campaign `json:"data"`
}

func (c *CampaignEnvelope) GetData() []*Campaign {
	return c.Data
}

// Campaigns define the pricing, budgeting, targeting and other info.
type Campaign struct {
	Id                   int64      `json:"id"`
	Name                 string     `json:"name"`
	Status               uint8      `json:"status"`
	Type                 string     `json:"type"`
	DateCreated          time.Time  `json:"date_created"`
	DateModified         time.Time  `json:"date_modified"`
	OptimizationStrategy string     `json:"optimization_strategy"`
	DateStart            time.Time  `json:"date_start"`
	DateEnd              time.Time  `json:"date_end"`
	DisplayURL           string     `json:"display_url"`
	Currency             string     `json:"currency"`
	LineItemId           Ref        `json:"line_item"`
	AdExchanges          []Ref      `json:"ad_exchanges"`
	Creatives            []Ref      `json:"creatives"`
	ConversionPixels     []Ref      `json:"conversion_pixels"`
	FunnelPixels         []Ref      `json:"funnel_pixels"`
	Pricing              Pricing    `json:"pricing"`
	Budget               Budget     `json:"budget"`
	Targeting            *Targeting `json:"targeting,omitempty"`
}

type Variable struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type Flash struct {
	BackupUrl       string     `json:"backup_url"`
	BackupUrlSecure string     `json:"backup_url_secure"`
	Variables       []Variable `json:"variables"`
}

type Creative struct {
	Id                int64  `json:"id"`
	HtmlContent       string `json:"html_content"`
	HtmlContentSecure string `json:"html_content_secure"`
	ResourceUrl       string `json:"resource_url"`
	ResourceUrlSecure string `json:"resource_url_secure"`
	ClickUrl          string `json:"click_url"`
	Type              string `json:"type"`
	Secure            bool   `json:"is_secure"`
	Flash             Flash  `json:"flash"`
	Width             int64  `json:"width"`
	Height            int64  `json:"height"`
	Campaigns         []Ref  `json:"campaigns"`
}

func (c *Creative) AdMarkup() string {
	switch c.Type {

	case "flash":

	case "html":
		return c.HtmlContent

	case "image":

	}
	return ""
}

type Pricing struct {
	Type      string  `json:"type"`
	Amount    float64 `json:"amount"`
	AmountMax float64 `json:"amount_max"`
}

type Budget struct {
	Type     string  `json:"type"`
	Pacing   string  `json:"pacing"`
	Total    float64 `json:"total"`
	DailyCap float64 `json:"daily_cap"`
}

type Ref struct {
	Id int64 `json:"id"`
}

type Targeting struct {
	Hours        []int        `json:"hours,omitempty"`
	Weekdays     []string     `json:"weekdays,omitempty"`
	FrequencyCap FrequencyCap `json:"frequency_cap"`
	DomainLists  IntFilter    `json:"domain_lists,omitempty"`
	Domains      StringFilter `json:"domains,omitempty"`
	Geo          Geo          `json:"geo,omitempty"`
	Browser      IntFilter    `json:"browser,omitempty"`
	Os           IntFilter    `json:"operating_system,omitempty"`
	Languages    StringFilter `json:"languages,omitempty"`
}

type FrequencyCap struct {
	Hours uint8 `json:"hours"`
	Count uint8 `json:"count"`
}

type DomainList struct {
	Id      int64    `json:"id"`
	Name    string   `json:"name"`
	Domains []string `json:"domains"`
}

type Filter struct {
	Include []interface{}
	Exclude []interface{}
}

type StringFilter struct {
	Filter
	Include []string `json:"include,omitempty"`
	Exclude []string `json:"exclude,omitempty"`
}

type IntFilter struct {
	Filter
	Include []int64 `json:"include,omitempty"`
	Exclude []int64 `json:"exclude,omitempty"`
}

func (f *Filter) Data() []interface{} {
	if f.Includes() {
		return f.Include
	}
	return f.Exclude
}

func (f *Filter) Includes() bool {
	return len(f.Include) > 0
}

func (f *Filter) Excludes() bool {
	return !f.Includes()
}

func (f *Filter) Bool() bool {
	return f.Includes()
}

func (f *Filter) Targets(value string) (bool, error) {
	if len(f.Data()) == 0 {
		return true, errors.New("Malformed filter data")
	}
	for _, v := range f.Data() {
		if v == value {
			return f.Bool(), nil
		}
	}
	return !f.Bool(), nil
}

type Geo struct {
	Include map[string][]string `json:"include,omitempty"`
	Exclude map[string][]string `json:"exclude,omitempty"`
	Filter
}

func (f *Geo) Data() map[string][]string {
	if f.Includes() {
		return f.Include
	}
	return f.Exclude
}

func (g *Geo) Targets(country string, region string) (bool, error) {
	if len(g.Data()) == 0 {
		return true, errors.New("Malformed filter data")
	}
	for c, regions := range g.Data() {
		if c == country {
			if len(regions) == 0 {
				return g.Bool(), nil
			} else {
				for _, r := range regions {
					if r == region {
						return g.Bool(), nil
					}
				}
				return !g.Bool(), nil
			}
		}
	}
	return !g.Bool(), nil
}
