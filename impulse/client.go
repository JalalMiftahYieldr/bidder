package impulse

import (
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	//"text/template"
)

const apiHost = "https://api-staging.ydworld.com/v2.0"

type Impulse struct {
	*http.Client
	Creatives []*Creative
	Campaigns []*Campaign
}

func New() *Impulse {
	transport := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	impulse := new(Impulse)
	impulse.Client = &http.Client{Transport: transport}

	return impulse
}

// Calls the Impulse API and gets all campaigns
func (impulse *Impulse) Sync() {
	impulse.SyncCampaigns()
}

func (impulse *Impulse) SyncCampaigns() {
	offset := 0
	//for {
	req, err := http.NewRequest("GET", apiHost+"/campaigns?status=1&limit=2&offset="+strconv.Itoa(offset), nil)
	if err != nil {
		log.Fatalln(err.Error())
	}
	req.SetBasicAuth("chucknorris", "dontneedone")

	log.Println("GET " + req.URL.String())

	res, err := impulse.Do(req)
	if err != nil {
		log.Fatalln(err.Error())
	}

	log.Println(res.Status)

	// Read raw request body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalln(err.Error())
	}
	res.Body.Close()

	// Unmarshal JSON body into an impulse.Envelope
	env := new(CampaignEnvelope)
	err = json.Unmarshal(body, env)
	if err != nil {
		log.Fatalln(err.Error())
	}

	impulse.Campaigns = append(impulse.Campaigns, env.GetData()...)

	if env.Meta.Received.To >= env.Meta.Total-1 {
		log.Println("All campaigns fetched")
		//break
	}

	for _, campaign := range env.GetData() {
		impulse.SyncCampaign(campaign)
		for _, creative := range campaign.Creatives {
			impulse.SyncCreative(creative.Id)
		}
		log.Println(strconv.Itoa(int(campaign.Id)) + " - " + campaign.Name)
	}

	offset = env.Meta.Received.To + 1
	//}
}

func (impulse *Impulse) SyncCampaign(campaign *Campaign) {
	req, err := http.NewRequest("GET", apiHost+"/campaigns/"+strconv.Itoa(int(campaign.Id)), nil)
	if err != nil {
		log.Fatalln(err.Error())
	}
	req.SetBasicAuth("chucknorris", "dontneedone")

	log.Println("GET " + req.URL.String())

	res, err := impulse.Do(req)
	if err != nil {
		log.Fatalln(err.Error())
	}

	log.Println(res.Status)

	// Read raw request body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalln(err.Error())
	}
	res.Body.Close()

	// Unmarshal JSON body into an impulse.Envelope
	err = json.Unmarshal(body, campaign)
	if err != nil {
		log.Fatalln(err.Error())
	}

	//log.Println(campaign)
}

func (impulse *Impulse) SyncCreative(id int64) {

	req, err := http.NewRequest("GET", apiHost+"/creatives/"+strconv.Itoa(int(id)), nil)
	if err != nil {
		log.Fatalln(err.Error())
	}
	req.SetBasicAuth("chucknorris", "dontneedone")

	log.Println("GET " + req.URL.String())

	res, err := impulse.Do(req)
	if err != nil {
		log.Fatalln(err.Error())
	}

	//log.Println(res.Status)

	// Read raw request body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalln(err.Error())
	}
	res.Body.Close()

	// Unmarshal JSON body into an impulse.Envelope
	creative := new(Creative)
	err = json.Unmarshal(body, creative)
	if err != nil {
		log.Fatalln(err.Error())
	}

	impulse.Creatives = append(impulse.Creatives, creative)

	//log.Println(creative)

}
